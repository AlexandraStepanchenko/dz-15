<?php


Route::get('/', 'PageController@page');
Route::get('/product', 'ProductController@product');
Route::get('/order', 'OrderController@order');

Route::get('/page/create','PageController@create');
Route::get('/pages/{id}/delete','PageController@delete');


Route::get('/product/create','ProductController@create');
Route::get('/products/{id}/delete','ProductController@delete');

Route::resources([
    'categories' => 'CategoriesController',
    'products' => 'ProductController',
    'pages' => 'PageController',
]);


/*Route::post('/pages','PageController@store');
Route::get('/pages/{id}', 'PageController@show');
Route::get('/pages/{id}/edit', 'PageController@edit');
Route::patch('/pages/{id}','PageController@update');
Route::delete('/pages/{page}','PageController@destroy');


Route::post('/products','ProductController@store');
Route::get('/products/{id}', 'ProductController@show');
Route::get('/products/{id}/edit', 'ProductController@edit');
Route::patch('/products/{id}','ProductController@update');
Route::delete('/products/{product}','ProductController@destroy');*/

