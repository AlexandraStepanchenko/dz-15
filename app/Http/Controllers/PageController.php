<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public  function page ()
    {
        $data['pages'] = Page::all();
        return view('pages', $data);
    }

    public function create(){
        return view('pages.create');
    }

    public  function store()
    {
        $this->validate(request(),[
            'title' => 'required|min:4|unique:pages,title',
            'alias' => 'required|min:4',
            'intro' => 'required|min:10|max:80',
            'content' => 'required|min:20'
        ]);

        Page::create(request(['title','alias','intro','content']));
        return redirect('/');
    }

    public function edit($id)
    {
       $data['page'] = Page::find($id);
       return view('pages.edit', $data);
    }

    public function update($id)
    {
        $this->validate(request(),[
            'title' => 'required|min:4',
            'alias' => 'required',
            'intro' => 'required',
            'content' => 'required'
        ]);

        $page = Page::find($id);
        $page->update(request(['title', 'alias', 'intro', 'content']));

        return redirect('/');
    }

    public  function delete($id){
        $data['page'] = Page::find($id);
        return view('pages.delete', $data);
    }

    public function destroy($id)
    {
        $data = Page::find($id);
        $data->delete();
        return redirect('/');

    }


}
