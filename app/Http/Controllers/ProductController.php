<?php

namespace App\Http\Controllers;

use App\Product;

use Illuminate\Http\Request;


class ProductController extends Controller
{
   public function product (){

        $data['products'] = Product::all();
        return view('products', $data);
    }

   public  function  show($id){

        $data['product'] = Product::find($id);
        return view('products.show', $data );
    }

    public function create(){

       return view('products.create');
    }

    public  function store(){

         $this->validate(request(),[
            'title' => 'required|min:4|unique:products,title',
            'alias' => 'required',
            'price' => 'required',
            'description' => 'required|min:20'
        ]);

        Product::create(request(['title','alias','price','description']));
        return redirect('/product');
    }

    public  function edit($id){

        $data['product'] = Product::find($id);
        return view('products.edit', $data);
    }

    public function update($id)
    {

        $this->validate(request(), [
            'title' => 'required|min:4|unique:products,title',
            'alias' => 'required',
            'price' => 'required',
            'description' => 'required|min:10'
        ]);

    $product = Product::find($id);
   $product->update(request(['title', 'alias', 'price', 'description']));


    return redirect('/product');
    }

    public  function delete($id){
        $data['product'] = Product::find($id);
        return view('products.delete', $data);
    }

    public function destroy($id)
    {
        $data = Product::find($id);
        $data->delete();
        return redirect('/product');

    }
}
