
@extends('admin/template')

@section('content')

    <div class="col-md-12">

        <form action="/admin/products" method="post" class="form-horizontal">

        @include('embed.errors')

            {{csrf_field()}}     {{-- проверка отправки формы с о странички сайта--}}

            <div class="form-group">

                <label for="title">Название:</label>
                <input type="text" name="title" id="title" class="form-control">

            </div>

            <div class="form-group">

                <label for="alias">Алиас:</label>
                <input type="text" name="alias" id="alias" class="form-control">

            </div>

            <div class="form-group">

                <label for="price">Цена:</label>
                <input type="text" name="price" id="price" class="form-control">

            </div>

            <div class="form-group">

                <label for="description">Описание:</label>
                <textarea  name="description" id="description" class="form-control"></textarea>
            </div>

            <div class="form-group">

                <button class="btn btn-default">Сохранить</button>
            </div>

        </form>
    </div>

@endsection

@section('jumbotron')

    <div class="jumbotron">
        <div class="container">
            <h1 class="display-4">Добавить товар:</h1>
        </div>
    </div>
@endsection
