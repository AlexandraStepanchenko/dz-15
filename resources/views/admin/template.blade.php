<head>
    <title>
        Smart Admin Dashboard by Mushfiq  - Dribbble
    </title>
    <meta name="description" content="Great work from a designer in the Dribbble community; your best resource to discover and connect with designers worldwide.">
    <meta charset="utf-8">
    <script type="text/javascript" src="https://bam.nr-data.net/1/7840d0c136?a=2909452&amp;v=1071.385e752&amp;to=Jg1bREUOClsARUpHABcHUF5ECQlDFhgWXAwS&amp;rst=4319&amp;ref=https://dribbble.com/shots/2041516-Smart-Admin-Dashboard&amp;ap=33&amp;be=875&amp;fe=3941&amp;dc=1940&amp;perf=%7B%22timing%22:%7B%22of%22:1517881425730,%22n%22:0,%22f%22:26,%22dn%22:102,%22dne%22:256,%22c%22:256,%22s%22:312,%22ce%22:408,%22rq%22:409,%22rp%22:669,%22rpe%22:724,%22dl%22:682,%22di%22:1939,%22ds%22:1939,%22de%22:1958,%22dc%22:3939,%22l%22:3939,%22le%22:3942%7D,%22navigation%22:%7B%7D%7D&amp;jsonp=NREUM.setToken"></script><script src="https://js-agent.newrelic.com/nr-1071.min.js"></script><script type="text/javascript" async="" src="https://cdn.segment.com/analytics.js/v1/NebmEAHRg0JSoZUQ3dvYl38FE7sSSTQi/analytics.min.js"></script><script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","licenseKey":"7840d0c136","applicationID":"2909452","transactionName":"Jg1bREUOClsARUpHABcHUF5ECQlDFhgWXAwS","queueTime":0,"applicationTime":33,"agent":""}</script>
    <script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,t,n){function r(n){if(!t[n]){var o=t[n]={exports:{}};e[n][0].call(o.exports,function(t){var o=e[n][1][t];return r(o||t)},o,o.exports)}return t[n].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<n.length;o++)r(n[o]);return r}({1:[function(e,t,n){function r(){}function o(e,t,n){return function(){return i(e,[f.now()].concat(u(arguments)),t?null:this,n),t?void 0:this}}var i=e("handle"),a=e(2),u=e(3),c=e("ee").get("tracer"),f=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,t){s[t]=o(d+t,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),t.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,t){var n={},r=this,o="function"==typeof t;return i(l+"tracer",[f.now(),e,n],r),function(){if(c.emit((o?"":"no-")+"fn-start",[f.now(),r,o],n),o)try{return t.apply(this,arguments)}catch(e){throw c.emit("fn-err",[arguments,this,e],n),e}finally{c.emit("fn-end",[f.now()],n)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,t){m[t]=o(l+t)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,f.now()])}},{}],2:[function(e,t,n){function r(e,t){var n=[],r="",i=0;for(r in e)o.call(e,r)&&(n[i]=t(r,e[r]),i+=1);return n}var o=Object.prototype.hasOwnProperty;t.exports=r},{}],3:[function(e,t,n){function r(e,t,n){t||(t=0),"undefined"==typeof n&&(n=e?e.length:0);for(var r=-1,o=n-t||0,i=Array(o<0?0:o);++r<o;)i[r]=e[t+r];return i}t.exports=r},{}],4:[function(e,t,n){t.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,t,n){function r(){}function o(e){function t(e){return e&&e instanceof r?e:e?c(e,u,i):i()}function n(n,r,o,i){if(!d.aborted||i){e&&e(n,r,o);for(var a=t(o),u=m(n),c=u.length,f=0;f<c;f++)u[f].apply(a,r);var p=s[y[n]];return p&&p.push([b,n,r,a]),a}}function l(e,t){v[e]=m(e).concat(t)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(n)}function g(e,t){f(e,function(e,n){t=t||"feature",y[n]=t,t in s||(s[t]=[])})}var v={},y={},b={on:l,emit:n,get:w,listeners:m,context:t,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",c=e("gos"),f=e(2),s={},p={},d=t.exports=o();d.backlog=s},{}],gos:[function(e,t,n){function r(e,t,n){if(o.call(e,t))return e[t];var r=n();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,t,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[t]=r,r}var o=Object.prototype.hasOwnProperty;t.exports=r},{}],handle:[function(e,t,n){function r(e,t,n,r){o.buffer([e],r),o.emit(e,t,n)}var o=e("ee").get("handle");t.exports=r,r.ee=o},{}],id:[function(e,t,n){function r(e){var t=typeof e;return!e||"object"!==t&&"function"!==t?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");t.exports=r},{}],loader:[function(e,t,n){function r(){if(!x++){var e=h.info=NREUM.info,t=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&t))return s.abort();f(y,function(t,n){e[t]||(e[t]=n)}),c("mark",["onload",a()+h.offset],null,"api");var n=d.createElement("script");n.src="https://"+e.agent,t.parentNode.insertBefore(n,t)}}function o(){"complete"===d.readyState&&i()}function i(){c("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),c=e("handle"),f=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,SI:p.setImmediate,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1071.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=t.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),c("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script>
    <meta name="theme-color" content="#333333">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <meta name="referrer" content="origin-when-cross-origin">
    <!--[if gte IE 7]><!-->
    <link rel="stylesheet" media="screen, projection" href="https://cdn.dribbble.com/assets/master-db034ecf2540bae891d42623cb778c3c3b1f1f4cf6c681a8351c551ee0373e12.css">
    <link rel="stylesheet" media="screen, projection" href="https://cdn.dribbble.com/assets/infinite-076624da9ec905a875e10ad4be445c3511295c5baa04f72695f24fa97060d19e.css"><link rel="stylesheet" media="screen, projection" href="https://cdn.dribbble.com/assets/color-browse-ec450c55b6448bcaeb7ae6837690734834c895de1feb1e7300cc7c1b483e5e17.css">
    <link rel="stylesheet" media="screen, projection" href="https://cdn.dribbble.com/assets/attachment-overlay-7a12596ba47e2a470ef6ea3482cd3de494d58e892359be13d57bab05b4f8efa8.css">
    <!-- <![endif]-->
    <link rel="stylesheet" media="print" href="https://cdn.dribbble.com/assets/print-af897c350716ce875940317977d37c5289e6aa39cb324abab961d4b514bd61f2.css">
    <link href="https://cdn.dribbble.com/assets/dribbble-vector-ball-95ca193529993e9b3904f7adb541680aba196642f557cfaf3cdbb0a48a284be4.svg" rel="mask-icon" color="#ea4c89">
    <link href="https://cdn.dribbble.com/assets/apple-touch-icon-precomposed-4a188d3f4dd693b67971fda843fd4bdeae751f9a245e3b878cae17e0a9d6a0a3.png" rel="apple-touch-icon-precomposed">
    <link href="https://cdn.dribbble.com/assets/favicon-63b2904a073c89b52b19aa08cebc16a154bcf83fee8ecc6439968b1e6db569c7.ico" rel="icon">
    <link href="https://cdn.dribbble.com/assets/dribbble-ball-192-ec064e49e6f63d9a5fa911518781bee0c90688d052a038f8876ef0824f65eaf2.png" sizes="192x192" rel="icon">


    <meta name="csrf-param" content="authenticity_token">
    <meta name="csrf-token" content="0O1ExJ3jgVFHjNOA1X4/5pJcuYBXUcv3tcispKJTGfD86LuRJHTcumdZA9jp2Pxx3KDG6LDS8X8YEYacibBcdw==">
    <meta name="description" content="I'm not so good in back-end design as most of the time I've done app and web front end based designs. So trying to learn back-end design :D

You can Download this PSD  if you want

Follow me on beh...">
    <meta property="og:description" content="I'm not so good in back-end design as most of the time I've done app and web front end based designs. So trying to learn back-end design :D

You can Download this PSD  if you want

Follow me on beh...">
    <meta name="twitter:description" content="I'm not so good in back-end design as most of the time I've done app and web front end based designs. So trying to learn back-end design :D

You can Download this PSD  if you want

Follow me on beh...">
    <meta property="og:type" content="article">
    <meta property="og:title" content="Smart Admin Dashboard">
    <meta name="twitter:title" content="Smart Admin Dashboard">
    <meta property="og:site_name" content="Dribbble">
    <meta name="twitter:site" content="@dribbble">
    <meta name="twitter:domain" content="dribbble.com">
    <meta property="og:url" content="https://dribbble.com/shots/2041516-Smart-Admin-Dashboard">
    <meta name="twitter:url" content="https://dribbble.com/shots/2041516-Smart-Admin-Dashboard">
    <meta name="twitter:creator" content="@Mushfiqislam19">
    <meta name="twitter:card" content="summary_large_image">
    <meta property="og:image" content="https://cdn.dribbble.com/users/473136/screenshots/2041516/untitled-1.jpg">
    <meta name="twitter:image" content="https://cdn.dribbble.com/users/473136/screenshots/2041516/untitled-1.jpg">
    <meta name="twitter:label1" content="Dribbbled by">
    <meta name="twitter:data1" content="@mushfiqsCreation">


    <meta name="apple-itunes-app" content="app-id=1228326440, app-argument=https://dribbble.com/shots/2041516-Smart-Admin-Dashboard">

    <script>
        if (location.hash.match(/^#\./) && window.history) {
            window.history.replaceState({}, window.title, location.origin + location.pathname + location.search)
        }
    </script>
    <noscript>
        &lt;style&gt;ol.dribbbles { display: block !important }&lt;/style&gt;
    </noscript>
    <script id="_carbonads_projs" type="text/javascript" src="//srv.carbonads.net/ads/CVAIP5QU.json?segment=placement:dribbble&amp;callback=_carbonads_go"></script></head>