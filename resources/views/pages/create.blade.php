
@extends('template')

@section('content')

    <div class="col-md-12">

        <form action="/pages" method="post" class="form-horizontal">

            {{csrf_field()}}

            <div class="form-group">

                <label for="title">Название:</label>
                <input type="text" name="title" id="title" class="form-control">

            </div>

            <div class="form-group">

                <label for="alias">Алиас:</label>
                <input type="text" name="alias" id="alias" class="form-control">

            </div>

            <div class="form-group">

                <label for="intro">Превью:</label>
                <input type="text" name="intro" id="intro" class="form-control">

            </div>

            <div class="form-group">

                <label for="content">Описание:</label>
                <textarea  name="content" id="content" class="form-control"></textarea>
            </div>

            <div class="form-group">
                <button class="btn btn-default">Сохранить</button>
            </div>

        </form>
    </div>

@endsection

@section('jumbotron')

    <div class="jumbotron">
        <div class="container">
            <h1 class="display-5">Добавить новый раздел:</h1>
        </div>
    </div>
@endsection
