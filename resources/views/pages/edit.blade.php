
@extends('template')

@section('content')

    <div class="col-md-12">

        <form action="/pages/{{$page->id}}" method="post" class="form-horizontal">

            {{csrf_field()}}

            {{method_field('PATCH')}}

            <div class="form-group">

                <label for="title">Название:</label>
                <input value="{{ $page->title }}" type="text" name="title" id="title" class="form-control">

            </div>

            <div class="form-group">

                <label for="alias">Алиас:</label>
                <input value="{{ $page->alias }}" type="text" name="alias" id="alias" class="form-control">

            </div>

            <div class="form-group">

                <label for="intro">Превью:</label>
                <textarea  name="intro" id="intro" class="form-control"> {{ $page->intro }}</textarea>

            </div>

            <div class="form-group">

                <label for="content">Описание:</label>
                <textarea  name="content" id="content" class="form-control">{{ $page->content }}</textarea>
            </div>

            <div class="form-group">
                <button class="btn btn-default">Сохранить</button>
            </div>

        </form>
    </div>

@endsection

@section('jumbotron')

    <div class="jumbotron">
        <div class="container">
            <h1 class="display-5">Изменить запись в разделе:</h1>
        </div>
    </div>

@endsection
