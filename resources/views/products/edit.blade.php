@extends('template')

@section('content')

    <div class="col-md-12">

        <form action="/products/{{$product->id}}" method="post" class="form-horizontal">

            @include('embed.errors')

            {{csrf_field()}}  {{-- формирует уникальный код, для каждого запроса. отвечает за то, что б данные отправлялись именно с данной странички.--}}

            {{method_field('PATCH')}}

            <div class="form-group">

                <label for="title">Название:</label>
                <input value="{{ $product->title }}" type="text" name="title" id="title" class="form-control">

            </div>

            <div class="form-group">

                <label for="alias">Алиас:</label>
                <input  value="{{ $product->alias }}" type="text" name="alias" id="alias" class="form-control">

            </div>

            <div class="form-group">

                <label for="price">Цена:</label>
                <input value="{{ $product->price }}" type="text" name="price" id="price" class="form-control">

            </div>

            <div class="form-group">

                <label for="description">Описание:</label>
                <textarea  name="description" id="description" class="form-control">{{ $product->description }}</textarea>
            </div>

            <div class="form-group">

                <button class="btn btn-default">Сохранить</button>
            </div>

        </form>
    </div>

@endsection

@section('jumbotron')

    <div class="jumbotron">
        <div class="container">
            <h1 class="display-4">Изменить товар:</h1>
        </div>
    </div>
@endsection

