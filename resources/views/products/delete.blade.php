@extends('template')

@section('content')

    <div class="col-md-12">

        <form action="/products/{{ $product->id }}" method="post" class="form-horizontal">

            @include('embed.errors')

            {{csrf_field()}}  {{-- формирует уникальный код, для каждого запроса. отвечает за то, что б данные отправлялись именно с данной странички.--}}

            <input type="hidden" name="_method" value="DELETE">

            <div class="form-group">

                <h3>Вы действительно хотите удалить запись {{ $product->title }}?</h3>

            </div>

            <div class="form-group">

                <button class="btn btn-danger">Удалить</button>

            </div>

        </form>
    </div>

@endsection

@section('jumbotron')

    <div class="jumbotron">
        <div class="container">
            <h1 class="display-4">Удалить товар:</h1>
        </div>
    </div>
@endsection
