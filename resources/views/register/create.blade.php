
@extends('template')

@section('jumbotron')

    <div class="jumbotron">
        <div class="container">
            <h1 class="display-4">Войти:</h1>
        </div>
    </div>
@endsection

@section('content')

    <div class="col-md-12">

        <form method="POST" action="/sessions">

            {{csrf_field()}}

            @include('embed.errors')

            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" class="form-control" id="email" name="email">
            </div>

            <div class="form-group">
                <label for="password">Пароль:</label>
                <input type="password" class="form-control" id="password" name="password">
            </div>

            <div class="form-group">
                <button class="btn btn-primary" type="submit">Войти</button>
            </div>

        </form>

    </div>
@endsection